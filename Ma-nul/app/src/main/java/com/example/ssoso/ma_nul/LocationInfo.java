package com.example.ssoso.ma_nul;

import android.util.Log;

import static android.content.ContentValues.TAG;

/**
 * Created by User on 2017-11-22.
 */

public class LocationInfo {
    private static String location;
    private static String viewLocation;

    public static String getViewLocation() {
        return viewLocation;
    }

    public static void setViewLocation(String viewLocation) {
        LocationInfo.viewLocation = viewLocation;
    }

    public static void setLocation(String lc){ location = lc;
        Log.d(TAG, "setLocation: " + location); }
    public static String getLocation() {return location;}
}
