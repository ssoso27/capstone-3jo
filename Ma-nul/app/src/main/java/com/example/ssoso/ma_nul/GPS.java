package com.example.ssoso.ma_nul;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by ssoso on 2017-11-24.
 */

public class GPS {
    private Activity activity;
    double lat, lng;

    public GPS(Activity activity)
    {
        this.activity = activity;
    }

    //주소 표시
    public void GetAddress(){
        String nowAddress ="현재 위치를 확인 할 수 없습니다.";
        GpsInfo gps = new GpsInfo(activity);
        //퍼미션 설정
        if ( ContextCompat.checkSelfPermission( activity, android.Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(activity, new String[] {  android.Manifest.permission.ACCESS_COARSE_LOCATION  }, 1);
        }
        Geocoder geocoder = new Geocoder(activity, Locale.KOREA);
        List<Address> address = null;

        if (gps.isGetLocation()) {
            lat = gps.getLatitude();
            lng = gps.getLongitude();
            ;
        } else {
            // GPS 설정이 꺼져 있을 때
            gps.showSettingsAlert();
        }
        try {
            if (geocoder != null) {
                address = geocoder.getFromLocation(lat, lng, 1);

                if (address != null && address.size() > 0) {
                    // 주소 받아오기
                    String currentLocationAddress = address.get(0).getAddressLine(0).toString();
                    nowAddress  = currentLocationAddress;

                }
            }

        } catch (IOException e) {
            Toast.makeText(activity, "주소를 가져 올 수 없습니다.", Toast.LENGTH_LONG).show();

            e.printStackTrace();
        }
//        return "안녕";
//        return "위도는" + lat + " / 경도는" + lng;
        LocationInfo.setViewLocation(nowAddress);
        changeLocation(nowAddress);

//        return nowAddress;
    }

    public static String getAddress(Context mContext, double lat, double lng) {
        String nowAddress ="현재 위치를 확인 할 수 없습니다.";
        Geocoder geocoder = new Geocoder(mContext, Locale.KOREA);
        List <Address> address;
        try {
            if (geocoder != null) {
                address = geocoder.getFromLocation(lat, lng, 1);

                if (address != null && address.size() > 0) {
                    // 주소 받아오기
                    String currentLocationAddress = address.get(0).getAddressLine(0).toString();
                    nowAddress  = currentLocationAddress;

                }
            }

        } catch (IOException e) {
            Toast.makeText(mContext, "주소를 가져 올 수 없습니다.", Toast.LENGTH_LONG).show();

            e.printStackTrace();
        }
        return nowAddress;
    }

    // 실제 주소를 관측소 찾기용 간단주소로 만드는 메소드
    private void changeLocation(String addr)
    {
        String simpleAddr;

        if ( isInclude(addr, "서귀포시") && isInclude(addr, "동") )
        {
            simpleAddr = "동홍동";
        } else if (isInclude(addr, "남원읍") || isInclude(addr, "성산읍")
                || isInclude(addr, "조천읍") || isInclude(addr, "구좌읍"))
        {
            simpleAddr = "성산읍";
        } else if (isInclude(addr, "읍") || isInclude(addr, "면")) {
            simpleAddr = "고산리";
        } else if (isInclude(addr, "연동") || isInclude(addr, "노형")
                || isInclude(addr, "외도") || isInclude(addr, "내도")
                || isInclude(addr, "이호") || isInclude(addr, "도두"))
        {
            simpleAddr = "연동";
        } else
        {
            simpleAddr = "이도동";
        }

        LocationInfo.setLocation(simpleAddr);
    }

    private boolean isInclude(String addr, String str)
    {
        boolean flag = false;

        if (addr.indexOf(str) == -1) flag = false;
        else flag = true;

        return flag;
    }

//    public static List<Address> JSONGeoAddress(double dLat, double dLon){
//        List<Address> GeoAddress = new ArrayList<Address>();
//        String url = "http://maps.google.co.kr/maps/api/geocod/json?latlng=" + dLat + "," + dLon + "&sensor=false";
//
//        return GeoAddress;
//    }
}
