package com.example.ssoso.ma_nul.Datas;

/**
 * Created by ssoso on 2017-11-23.
 */

public class Weather {
    private String category; // 카테고리
    private String fcstDate; // 예보일자
    private String fcstTime; // 예보시각
    private String fcstValue; // 예보 값
    private String nx; // 예보지점 X 좌표
    private String ny; // 예보지점 Y 좌표

    public Weather(String category, String fcstDate, String fcstTime, String fcstValue, String nx, String ny) {
        this.category = category;
        this.fcstDate = fcstDate;
        this.fcstTime = fcstTime;
        this.fcstValue = fcstValue;
        this.nx = nx;
        this.ny = ny;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getFcstDate() {
        return fcstDate;
    }

    public void setFcstDate(String fcstDate) {
        this.fcstDate = fcstDate;
    }

    public String getFcstTime() {
        return fcstTime;
    }

    public void setFcstTime(String fcstTime) {
        this.fcstTime = fcstTime;
    }

    public String getFcstValue() {
        return fcstValue;
    }

    public void setFcstValue(String fcstValue) {
        this.fcstValue = fcstValue;
    }

    public String getNx() {
        return nx;
    }

    public void setNx(String nx) {
        this.nx = nx;
    }

    public String getNy() {
        return ny;
    }

    public void setNy(String ny) {
        this.ny = ny;
    }
}
