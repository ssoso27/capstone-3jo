package com.example.ssoso.ma_nul;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by ssoso on 2017-11-12.
 */

// 기타 대기지수 설명
public class OtherAirFragment extends Fragment{

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_other_air, container, false);

        TextView tv_o3Con = (TextView) rootView.findViewById(R.id.tv_o3Con);
        TextView tv_coCon = (TextView) rootView.findViewById(R.id.tv_coCon);
        TextView tv_no2Con = (TextView) rootView.findViewById(R.id.tv_no2Con);
        TextView tv_so2Con = (TextView) rootView.findViewById(R.id.tv_so2Con);

        TextView tv_o3State = (TextView) rootView.findViewById(R.id.tv_o3State);
        TextView tv_coState = (TextView) rootView.findViewById(R.id.tv_coState);
        TextView tv_no2State = (TextView) rootView.findViewById(R.id.tv_no2State);
        TextView tv_so2State = (TextView) rootView.findViewById(R.id.tv_so2State);

        String o3Con = DataGetter.getO3Value();
        String coCon = DataGetter.getCOValue();
        String no2Con = DataGetter.getNO2Value();
        String so2Con = DataGetter.getSO2Value();

        String o3State = DataGetter.getO3Grade();
        String coState = DataGetter.getCOGrade();
        String no2State = DataGetter.getNO2Grade();
        String so2State = DataGetter.getSO2Grade();

        tv_o3Con.setText(o3Con);
        tv_coCon.setText(coCon);
        tv_no2Con.setText(no2Con);
        tv_so2Con.setText(so2Con);

        tv_o3State.setText(o3State);
        tv_coState.setText(coState);
        tv_no2State.setText(no2State);
        tv_so2State.setText(so2State);

        return rootView;
    }

}
