package com.example.ssoso.ma_nul;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by ssoso on 2017-11-12.
 */

// 초미세먼지 지수 설명
public class UltrafineDustFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_ultrafine_dust, container, false);

        TextView tv_ultraCon = (TextView) rootView.findViewById(R.id.tv_ultraCon);
        TextView tv_ultraState = (TextView) rootView.findViewById(R.id.tv_ulltraState);

        String con;
        String state;

        con = DataGetter.getUltraValue();
        state = DataGetter.getUltraGrade();

        tv_ultraCon.setText(con);
        tv_ultraState.setText(state);

        return rootView;
    }
}
