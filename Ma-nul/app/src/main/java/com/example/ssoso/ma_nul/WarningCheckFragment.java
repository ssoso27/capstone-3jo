package com.example.ssoso.ma_nul;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.example.ssoso.ma_nul.Datas.AirPollution;
import com.example.ssoso.ma_nul.Datas.DataArrays;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;

/**
 * Created by ssoso on 2017-12-13.
 */

public class WarningCheckFragment extends Fragment implements CompoundButton.OnCheckedChangeListener {
    private View rootView;
    private int user_checked;
    private int mask;
    private CheckBox[] cbs_fine, cbs_ultra;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_warning_check, container, false);

        cbs_fine = new CheckBox[4];
        cbs_ultra = new CheckBox[4];

        cbs_fine[0] = (CheckBox) rootView.findViewById(R.id.cb_fine_verybad);   cbs_ultra[0] = (CheckBox) rootView.findViewById(R.id.cb_ultra_verybad);
        cbs_fine[1] = (CheckBox) rootView.findViewById(R.id.cb_fine_bad);       cbs_ultra[1] = (CheckBox) rootView.findViewById(R.id.cb_ultra_bad);
        cbs_fine[2] = (CheckBox) rootView.findViewById(R.id.cb_fine_normal);    cbs_ultra[2] = (CheckBox) rootView.findViewById(R.id.cb_ultra_normal);
        cbs_fine[3] = (CheckBox) rootView.findViewById(R.id.cb_fine_good);      cbs_ultra[3] = (CheckBox) rootView.findViewById(R.id.cb_ultra_good);

        cbs_fine[0].setChecked(getFromSP("cb_fine_verybad"));   cbs_ultra[0].setChecked(getFromSP("cb_ultra_verybad"));
        cbs_fine[1].setChecked(getFromSP("cb_fine_bad"));      cbs_ultra[1].setChecked(getFromSP("cb_ultra_bad"));
        cbs_fine[2].setChecked(getFromSP("cb_fine_normal"));   cbs_ultra[2].setChecked(getFromSP("cb_ultra_normal"));
        cbs_fine[3].setChecked(getFromSP("cb_fine_good"));     cbs_ultra[3].setChecked(getFromSP("cb_ultra_good"));

        for (int i = 0; i < cbs_fine.length; i++)
        {
            cbs_fine[i].setOnCheckedChangeListener(this);
            cbs_ultra[i].setOnCheckedChangeListener(this);
        }

        return rootView;
    }

    private boolean getFromSP(String key){
        SharedPreferences preferences = getActivity().getApplicationContext().getSharedPreferences("PROJECT_NAME", android.content.Context.MODE_PRIVATE);
        return preferences.getBoolean(key, false);
    }
    private void saveInSp(String key,boolean value){
        SharedPreferences preferences = getActivity().getApplicationContext().getSharedPreferences("PROJECT_NAME", android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        // TODO Auto-generated method stub
        switch(buttonView.getId()){
            case R.id.cb_fine_verybad:
                saveInSp("cb_fine_verybad",isChecked);
                break;
            case R.id.cb_fine_bad:
                saveInSp("cb_fine_bad",isChecked);
                break;

            case R.id.cb_fine_normal:
                saveInSp("cb_fine_normal",isChecked);
                break;

            case R.id.cb_fine_good:
                saveInSp("cb_fine_good",isChecked);
                break;

            case R.id.cb_ultra_verybad:
                saveInSp("cb_ultra_verybad",isChecked);
                break;
            case R.id.cb_ultra_bad:
                saveInSp("cb_ultra_bad",isChecked);
                break;

            case R.id.cb_ultra_normal:
                saveInSp("cb_ultra_normal",isChecked);
                break;

            case R.id.cb_ultra_good:
                saveInSp("cb_ultra_good",isChecked);
                break;
        }
        this.user_checked = combineChecked();
        this.mask = getMask();

        PushNotification.push(getActivity(), this.mask);
    }

    private int combineChecked()
    {
        String str = "";

        for (int i = 0; i < cbs_fine.length; i++)
        {
            if (cbs_fine[i].isChecked()) str += "1";
            else str += "0";
        }

        for (int i = 0; i < cbs_ultra.length; i++)
        {
            if(cbs_ultra[i].isChecked()) str += "1";
            else str += "0";
        }

        return Integer.parseInt(str, 2);
    }

    private int getMask()
    {
        int binary_air = getBinaryAirInfo();

        int mask = this.user_checked & binary_air;

        Log.d(TAG, "getMask: " + mask + ", " + Integer.toBinaryString(mask));
        return mask;
    }

    private int getBinaryAirInfo()
    {
        String[] airState = findGrade();
        String binary_air = "";

        for (int i = 0; i < airState.length; i++)
        {
            switch (airState[i])
            {
                case "매우나쁨" :
                    binary_air += "1000";
                    break;

                case "나쁨" :
                    binary_air += "0100";
                    break;

                case "보통" :
                    binary_air += "0010";
                    break;

                case "좋음" :
                    binary_air += "0001";
                    break;

                default:
                    binary_air += "0000";
                    break;
            }
        }

        return Integer.parseInt(binary_air, 2);
    }

    // 등급 찾기
    private String[] findGrade()
    {
        ArrayList<AirPollution> airPollutions = DataArrays.getAirPollutions();
        String location = LocationInfo.getLocation();
        String[] str = new String[2];

        // 현재 지역에 맞는 미세먼지 농도 찾기
        for (AirPollution ap : airPollutions)
        {
            if (ap.getLocation().equals(location))
            {
                str[0] = ap.getPm10Grade();
                str[1] = ap.getPm25Grade();

                Log.d(TAG, "findGrade: " + str[0] + ", " +str[1]);
            }
        }

        return str;
    }
}
