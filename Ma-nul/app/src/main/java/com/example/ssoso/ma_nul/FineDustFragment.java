package com.example.ssoso.ma_nul;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by ssoso on 2017-11-12.
 */

// 미세먼지 지수 설명
public class FineDustFragment extends Fragment {
    View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_fine_dust, container, false);

        TextView tv_fineCon = (TextView) rootView.findViewById(R.id.tv_finecon);
        TextView tv_fineState = (TextView) rootView.findViewById(R.id.tv_finestate);

        String fineCon; // 미세먼지 농도
        String fineState; // 미세먼지 상태

        fineCon = DataGetter.getFineValue();
        fineState = DataGetter.getFineGrade();

        tv_fineCon.setText(fineCon);
        tv_fineState.setText(fineState);

        return rootView;
    }
}
