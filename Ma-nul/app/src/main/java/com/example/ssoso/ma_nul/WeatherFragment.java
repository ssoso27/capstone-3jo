package com.example.ssoso.ma_nul;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by ssoso on 2017-11-12.
 */

// 일주일 간의 날씨 알려줌
public class WeatherFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_weather, container, false);

        TextView tv_t3h = (TextView) rootView.findViewById(R.id.tv_t3h);
        TextView tv_wsd = (TextView) rootView.findViewById(R.id.tv_wsd);

        tv_t3h.setText(DataGetter.getTemperature());
        tv_wsd.setText(DataGetter.getWindSpeed());

        return rootView;
    }
}
