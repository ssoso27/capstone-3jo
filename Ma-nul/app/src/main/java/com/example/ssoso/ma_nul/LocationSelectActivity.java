package com.example.ssoso.ma_nul;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by User on 2017-11-22.
 */

public class LocationSelectActivity extends AppCompatActivity implements Button.OnClickListener {
    String locationName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_select);

        Button btn_Ido = (Button) findViewById(R.id.btn_Ido) ;
        btn_Ido.setOnClickListener(this);
        Button btn_Yeon = (Button) findViewById(R.id.btn_Yeon) ;
        btn_Yeon.setOnClickListener(this);
        Button btn_Dong = (Button) findViewById(R.id.btn_Dong) ;
        btn_Dong.setOnClickListener(this);
        Button btn_Seong = (Button) findViewById(R.id.btn_Seong) ;
        btn_Seong.setOnClickListener(this);
        Button btn_Go = (Button) findViewById(R.id.btn_Go) ;
        btn_Go.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        String str = "";

        switch (view.getId()){
            case R.id.btn_Ido :
                str = "이도동";
                break;
            case R.id.btn_Yeon :
                str = "연동";
                break;
            case R.id.btn_Dong :
                str = "동홍동";
                break;
            case R.id.btn_Seong :
                str = "성산리";
                break;
            case R.id.btn_Go :
                str = "고산리";
                break;
        }

        LocationInfo.setLocation(str);
        LocationInfo.setViewLocation(str);
        Log.d(getClass().toString(), "onClick: location, viewLocation : " + LocationInfo.getLocation() + LocationInfo.getViewLocation());
        Toast.makeText(getApplicationContext(), str + "(으)로 위치 변경", Toast.LENGTH_LONG).show();

    }



}
