package com.example.ssoso.ma_nul.Datas;

import java.util.ArrayList;

/**
 * Created by ssoso on 2017-11-24.
 */

public class DataArrays {
    private static ArrayList<AirPollution> airPollutions;
    private static ArrayList<Weather> weathers;

    public static ArrayList<AirPollution> getAirPollutions() {
        return airPollutions;
    }

    public static void setAirPollutions(ArrayList<AirPollution> airPollutions) {
        DataArrays.airPollutions = airPollutions;
    }

    public static ArrayList<Weather> getWeathers() {
        return weathers;
    }

    public static void setWeathers(ArrayList<Weather> weathers) {
        DataArrays.weathers = weathers;
    }
}
