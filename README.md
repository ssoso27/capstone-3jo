# 마늘
이용자에게 날씨에 관한 풍부한 종합 정보를 쉽게 전달  

---
## 마늘 프로젝트 소개  
마늘(마른 하늘) 프로젝트는 이용자에게 제주도 날씨에 관한 풍부한 종합 정보를 쉽게 전달하기 위한 프로젝트이다. 

- 메인화면에서는 GPS정보를 통하여 날씨, 기온, 미세먼지 농도를 표시해준다.  
- 네비게이션 바를 이용하여 다양한 정보를 제공해준다.  
   - 미세먼지, 초미세먼지, O₃ 등 대기오염물질의 경보 기준 제시  
     미세먼지, 초미세먼지, O₃ 등 대기오염물질의 농도에 따른 행동요령 제시  
- 미세먼지 경고 메시지를 받을 수 있다.  
- 위젯을 통하여 더욱 편리한 사용이 가능하다.  
---
## 간단 Git Bash 사용법
### 0. 지정 폴더 들어가기
- ls :: 현재 디렉터리의 파일 및 하위 디렉터리를 보여준다  
- cd <하위 디렉터리명> :: 하위 디렉터리로 이동한다  
- cd .. :: 상위 디렉터리로 이동한다  

### 1. git clone  
- Bitbucket에 올라와 있는 파일을 (현재 디렉터리에) 다운 받는 명령어  
- 최초 한 번만 하면 된다.  
- **사용법 :: git clone https://ssoso27@bitbucket.org/ssoso27/capstone-3jo.git**  
### 2. git pull  
- 현재 디렉터리의 프로젝트를 Bitbucket에 올라온 최신 버전으로 업데이트 하는 명령어  
- 항상 최신 버전을 유지하기 위해, 다른 팀원의 Pull request가 merge 된 후에는 git pull를 해야한다.  
- **사용법 :: git pull**
### 3. git checkout   
- 다른 branch 로 넘어가는 명령어  
- 자신의 브랜치에서 작업합시다 여러분  
- master에서 작업하다 적발되면 나한테 머리뽑힌다  
- **사용법 :: git checkout <브랜치 이름> **  
- ex) git checkout ysh
### 4. git add
- 파일을 준비 영역에 추가하는 명령어  
- **사용법 :: git add <파일 또는 폴더명>**  
- ex) git add YSH/  
- ex) git add test.cpp  
### 5. git commit  
- (git add로 추가된) 준비 영역의 파일을 최종 확정하는 명령어  
- **사용법 :: git commit -m "파일에 대한 설명 문구"**  
- ex) git commit -m "커밋 테스트"  
### 6. git push
- (git commit으로 확정된) 파일을 원격 서버에 올리는 명령어  
- 아마 처음엔 로그인 하라고 에러 메시지가 나올텐데, 침착하게 자기 아이디랑 비번을 입력하면 된다.  
- **사용법 :: git push**  
### 7. git status  
- 현재 폴더의 상태 확인  
- 수정된 파일 등을 나타냄   
- **사용법 :: git status**  
### 8. Pull request  
#### ( git push 이후 )  
- 자신의 업로드 내역을 적용시키기 위해 요청해야함   
- 웬만하면 기능 하나 구현 후 해주세욤
- 1) Bitbucket Repository에 들어온다
- 2) 좌측 pull requests 클릭
- 3) 우측 상단 Create pull request 클릭
- 4) Title : 기능이름 / Description : 간단하게 설명
### 9. 누군가의 업로드 이후  
#### ( = SM이 merge 했다고 한 이후 할 것)  
- 1) git checkout master (master 브랜치로 이동)  
- 2) git pull (다른 사람이 구현한거 받음)  
- 3) git checkout <자기 브랜치> (자기 브랜치로 이동)  
- 4) git merge master (자기 브랜치에도 업데이트)   

### 석 줄 요약  
- 한 번도 clone을 안 했을 경우, clone을 한다.  
- (자기 브랜치에서) **add -> commit -> push** 순으로 파일 업로드  
- SM이 merge 했다고 할 경우(혹은 pull 받아달라고 할 경우), **9번의 과정**을 해준다.  