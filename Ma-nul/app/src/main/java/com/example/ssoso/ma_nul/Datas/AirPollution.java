package com.example.ssoso.ma_nul.Datas;

/**
 * Created by ssoso on 2017-11-23.
 */

public class AirPollution {
    private String location;
    private String fineDust;
    private String ultraFine;
    private String COvalue;
    private String O3value;
    private String NO2value;
    private String SO2value;
    private String so2Grade;
    private String coGrade;
    private String o3Grade;
    private String no2Grade;
    private String pm10Grade;
    private String pm25Grade;

    public AirPollution(String location, String[] values, String[] grades) {
        this.location = location;
        this.fineDust = values[0];
        this.ultraFine = values[1];
        this.COvalue = values[2];
        this.O3value = values[3];
        this.NO2value = values[4];
        this.SO2value = values[5];
        this.so2Grade = grades[0];
        this.coGrade = grades[1];
        this.o3Grade = grades[2];
        this.no2Grade = grades[3];
        this.pm10Grade = grades[4];
        this.pm25Grade = grades[5];
    }

    public String getSo2Grade() {
        return so2Grade;
    }

    public void setSo2Grade(String so2Grade) {
        this.so2Grade = so2Grade;
    }

    public String getCoGrade() {
        return coGrade;
    }

    public void setCoGrade(String coGrade) {
        this.coGrade = coGrade;
    }

    public String getO3Grade() {
        return o3Grade;
    }

    public void setO3Grade(String o3Grade) {
        this.o3Grade = o3Grade;
    }

    public String getNo2Grade() {
        return no2Grade;
    }

    public void setNo2Grade(String no2Grade) {
        this.no2Grade = no2Grade;
    }

    public String getPm10Grade() {
        return pm10Grade;
    }

    public void setPm10Grade(String pm10Grade) {
        this.pm10Grade = pm10Grade;
    }

    public String getPm25Grade() {
        return pm25Grade;
    }

    public void setPm25Grade(String pm25Grade) {
        this.pm25Grade = pm25Grade;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getFineDust() {
        return fineDust;
    }

    public void setFineDust(String fineDust) {
        this.fineDust = fineDust;
    }

    public String getUltraFine() {
        return ultraFine;
    }

    public void setUltraFine(String ultraFine) {
        this.ultraFine = ultraFine;
    }

    public String getCOvalue() {
        return COvalue;
    }

    public void setCOvalue(String COvalue) {
        this.COvalue = COvalue;
    }

    public String getO3value() {
        return O3value;
    }

    public void setO3value(String o3value) {
        O3value = o3value;
    }

    public String getNO2value() {
        return NO2value;
    }

    public void setNO2value(String NO2value) {
        this.NO2value = NO2value;
    }

    public String getSO2value() {
        return SO2value;
    }

    public void setSO2value(String SO2value) {
        this.SO2value = SO2value;
    }
}
