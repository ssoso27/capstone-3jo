package com.example.ssoso.ma_nul;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.util.Log;

import com.example.ssoso.ma_nul.Datas.AirPollution;
import com.example.ssoso.ma_nul.Datas.DataArrays;

import java.util.ArrayList;

import static android.app.PendingIntent.FLAG_UPDATE_CURRENT;
import static android.content.ContentValues.TAG;

/**
 * Created by ssoso on 2017-12-13.
 */

public class PushNotification {
    Activity activity = null;
    ArrayList<AirPollution> airPollutions = null;

    public PushNotification(Activity activity)
    {
        this.activity = activity;
        airPollutions = DataArrays.getAirPollutions();
    }

    public static void push(Activity activity, int warning)
    {
        PushNotification pn = new PushNotification(activity);

        String pushMassage;
        String s_fine = "", s_ultra = "";

        String binary = lpad(Integer.toBinaryString(warning), 8, "0");
        String b_fine = binary.substring(0, 4);
        Log.d(TAG, "push: binary " + binary);
        String b_ultra = binary.substring(4, 8);

        if(pn.isWarning(b_fine)){
            s_fine += "미세먼지 농도는 ";
            s_fine += DataGetter.getFineGrade();

            if (pn.isWarning(b_ultra)){
                s_fine += "이고, ";
            }
        }
        if (pn.isWarning(b_ultra)){
            s_ultra += "초미세먼지 농도는 ";
            s_ultra += DataGetter.getUltraGrade();
        }

        if (!(s_fine.equals("") && s_ultra.equals("")))
        {
            pushMassage = "현재 " + s_fine + s_ultra + "입니다.";
            pn.pushNotification(pushMassage);
        }
    }

    public void push(int warning, int deadline)
    {

    }

    public static String lpad(String str, int len, String addStr) {
        String result = str;
        int templen   = len - result.length();

        for (int i = 0; i < templen; i++){
            result = addStr + result;
        }

        return result;
    }

    private boolean isWarning(String str) { return !str.equals("0000"); }

    private void pushNotification(String pushMassage)
    {
        NotificationManager notificationManager = (NotificationManager) activity.getSystemService(activity.NOTIFICATION_SERVICE);
        Intent intent1 = new Intent(activity.getApplicationContext(), activity.getClass()); //인텐트 생성.

        Notification.Builder builder = new Notification.Builder(activity.getApplicationContext());
        intent1.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);//현재 액티비티를 최상으로 올리고, 최상의 액티비티를 제외한 모든 액티비티를 없앤다.

        PendingIntent pendingNotificationIntent = PendingIntent.getActivity(activity, 0, intent1, FLAG_UPDATE_CURRENT);

        builder.setSmallIcon(R.drawable.on).setTicker("HETT").setWhen(System.currentTimeMillis())
                .setNumber(1).setContentText(pushMassage)
                .setDefaults(android.app.Notification.DEFAULT_SOUND | android.app.Notification.DEFAULT_VIBRATE).setContentIntent(pendingNotificationIntent).setAutoCancel(true).setOngoing(true);

        notificationManager.notify(1, builder.build()); // Notification send
    }
}
