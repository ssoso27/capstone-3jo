package com.example.ssoso.ma_nul;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.ssoso.ma_nul.Datas.AirPollution;
import com.example.ssoso.ma_nul.Datas.DataArrays;
import com.example.ssoso.ma_nul.Datas.Weather;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;


// 메인화면
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private final long FINISH_INTERVAL_TIME = 2000;
    private long   backPressedTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setNavigation();
        getSupportFragmentManager().beginTransaction().replace(R.id.frag_container, new InitialFragment()).commit();

    }



    // Toolbar, Navigation 세팅
    private void setNavigation()
    {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    // 뒤로가기
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        long tempTime = System.currentTimeMillis();
        long intervalTime = tempTime - backPressedTime;
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frag_container);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (fragment instanceof InitialFragment) {
                //          두 번 클릭 시 종료
                if (0 <= intervalTime && FINISH_INTERVAL_TIME >= intervalTime) {
                    super.onBackPressed();
                } else {
                    backPressedTime = tempTime;
                    Toast.makeText(getApplicationContext(), "한 번 더 누르면 종료됩니다.", Toast.LENGTH_SHORT).show();
                }
            } else {
                getSupportFragmentManager().beginTransaction().replace(R.id.frag_container, new InitialFragment()).commit();
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        FragmentTransaction trans = getSupportFragmentManager().beginTransaction();
        Fragment fragment = null;

        if (id == R.id.nav_main) {
            // Handle the camera action
            Log.d(this.getClass().toString(), "main 클릭");
            fragment = new InitialFragment();

        } else if (id == R.id.nav_fine_dust) {
            Log.d(this.getClass().toString(), "미세먼지 클릭");
            fragment = new FineDustFragment();

        } else if (id == R.id.nav_ultrafine_dust) {
            Log.d(this.getClass().toString(), "초미세먼지 클릭");
            fragment = new UltrafineDustFragment();

        } else if (id == R.id.nav_extraAtmosphere) {
            Log.d(this.getClass().toString(), "기타 대기 정보 클릭");
            fragment = new OtherAirFragment();

        } else if (id == R.id.nav_weather) {
            Log.d(this.getClass().toString(), "날씨 클릭");
            fragment = new WeatherFragment();

        } else if (id == R.id.nav_warning) {
            Log.d(this.getClass().toString(), "경보 설정 클릭");
            fragment = new WarningCheckFragment();

        }
        trans.replace(R.id.frag_container, fragment).commit();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    protected void onResume() {
        super.onResume();
    }//?




}
