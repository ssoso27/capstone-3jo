package com.example.ssoso.ma_nul;

import com.example.ssoso.ma_nul.Datas.AirPollution;
import com.example.ssoso.ma_nul.Datas.DataArrays;
import com.example.ssoso.ma_nul.Datas.Weather;

import java.util.ArrayList;

/**
 * Created by ssoso on 2017-12-13.
 */

public class DataGetter {

    public static String getFineValue() { return getMatchedAir(0); }
    public static String getUltraValue() { return getMatchedAir(1); }
    public static String getCOValue() { return getMatchedAir(2); }
    public static String getO3Value() { return getMatchedAir(3); }
    public static String getNO2Value() { return getMatchedAir(4); }
    public static String getSO2Value() { return getMatchedAir(5); }
    public static String getSO2Grade() { return getMatchedAir(6); }
    public static String getCOGrade() { return getMatchedAir(7); }
    public static String getO3Grade() { return getMatchedAir(8); }
    public static String getNO2Grade() { return getMatchedAir(9); }
    public static String getFineGrade() { return getMatchedAir(10); }
    public static String getUltraGrade() { return getMatchedAir(11); }

    // 현재 지역에 맞는 대기정보 값 get
    private static String getMatchedAir(int choice)
    {
        ArrayList<AirPollution> airPollutions = DataArrays.getAirPollutions();
        String location = LocationInfo.getLocation();
        String str = "정보 없음";

        for (AirPollution ap : airPollutions)
        {
            if (ap.getLocation().equals(location))
            {
                switch (choice)
                {
                    case 0: str = ap.getFineDust(); break;
                    case 1: str = ap.getUltraFine(); break;
                    case 2: str = ap.getCOvalue(); break;
                    case 3: str = ap.getO3value(); break;
                    case 4: str = ap.getNO2value(); break;
                    case 5: str = ap.getSO2value(); break;

                    case 6: str = ap.getSo2Grade(); break;
                    case 7: str = ap.getCoGrade(); break;
                    case 8: str = ap.getO3Grade(); break;
                    case 9: str = ap.getNo2Grade(); break;
                    case 10: str = ap.getPm10Grade(); break;
                    case 11: str = ap.getPm25Grade(); break;
                }
            }
        }

        if (str.equals("") || str.equals("-")) str="정보 없음";

        return str;
    }

    public static String getRainState() {
        String str = getMatchedWeather("PTY");
        String state = "정보 없음";

        switch (str)
        {
            case "0" : state = "없음"; break;
            case "1" : state = "비"; break;
            case "2" : state = "비/눈"; break;
            case "3" : state = "눈"; break;
        }

        return state;
    }
    public static String getSkyState() {
        String str = getMatchedWeather("SKY");
        String state = "";

        switch (str)
        {
            case "1" : state = "맑음"; break;
            case "2" : state = "구름조금"; break;
            case "3" : state = "구름많음"; break;
            case "4" : state = "흐림"; break;
        }

        return state;
    }
    public static String getMaxTemp() { return getMatchedWeather("TMX"); }
    public static String getMinTemp() { return getMatchedWeather("TMN"); }
    public static String getTemperature() { return getMatchedWeather("T3H"); }
    public static String getWindSpeed() { return getMatchedWeather("WSD"); }

    // 카테고리에 맞는 날씨 값 get
    private static String getMatchedWeather(String category)
    {
        ArrayList<Weather> weathers = DataArrays.getWeathers();
        String str = "";

        // 현재 카테고리의 value찾기
        for (Weather wt : weathers)
        {
            if (wt.getCategory().equals(category))
            {
                str = wt.getFcstValue();
            }
        }

        return str;
    }
}
