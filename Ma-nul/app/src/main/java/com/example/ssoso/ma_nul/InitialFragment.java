package com.example.ssoso.ma_nul;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.Random;
import static android.content.ContentValues.TAG;


/**
 * Created by ssoso on 2017-11-14.
 */

//첫화면
public class InitialFragment extends Fragment implements Button.OnClickListener {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_initial, container, false);

        GPS gps = new GPS(getActivity());

        TextView tv_address = (TextView)rootView.findViewById(R.id.user_address);
        TextView tv_recommand = (TextView) rootView.findViewById(R.id.tv_recommand);
        TextView tv_weather = (TextView) rootView.findViewById(R.id.tv_weather);


        Random random = new Random();
        int randomNumber = random.nextInt(9);
        String textRecommand[] =
                {"난방 등 연료의 사용은 미세먼지 농도를 높입니다.",
                "성층권의 오존은 좋은 오존, 지상의 오존은 나쁜 오존.",
                "오존 농도는 4~6월경 초여름에 많이 발생합니다.",
                "미세먼지가 빛을 흡수하여 가시거리가 감소합니다.",
                "2015년에는 지구촌 사망원인 5위가 대기오염입니다.",
                "오존은 기온이 높고 바람이 약할 때 많이 발생합니다.",
                "SO2는 무색에 자극적인 냄새가 나는 유독성 기체입니다.",
                "CO는 탄소 화합물이 불완전 연소되면 발생합니다.",
                "미세먼지보다 초미세먼지가 인체에 피해를 더 줍니다.",
                "NO2는 토양중의 세균에 의해 자연적으로 생성되도 합니다."};
        tv_recommand.setText(textRecommand[randomNumber]);

        tv_address.setText(LocationInfo.getViewLocation());
        tv_weather.setText(DataGetter.getTemperature() + "˚C");

        Button btn_locationSelecet = (Button) rootView.findViewById(R.id.btn_locationSelect);
        Button btn_refresh = (Button) rootView.findViewById(R.id.btn_refresh);

        Button btn_goWeather = (Button) rootView.findViewById(R.id.btn_goWeather);
        //날씨에 따라 날씨 아이콘 변경
        String skystate = DataGetter.getSkyState();
        String rainstate = DataGetter.getRainState();
//        btn_goWeather.setText(skystate);
        if(rainstate=="비") btn_goWeather.setBackgroundResource(R.drawable.rainy);
        else if(rainstate=="비/눈") btn_goWeather.setBackgroundResource(R.drawable.rainy);
        else if(rainstate=="눈") btn_goWeather.setBackgroundResource(R.drawable.snowy);
        else if(skystate=="맑음") btn_goWeather.setBackgroundResource(R.drawable.sunny);
        else if(skystate=="구름조금") btn_goWeather.setBackgroundResource(R.drawable.cloudy);
        else if(skystate=="구름많음") btn_goWeather.setBackgroundResource(R.drawable.cloud);
        else if(skystate=="흐림") btn_goWeather.setBackgroundResource(R.drawable.cloud);
        else btn_goWeather.setText("데이터 없음");

        Button btn_goFine = (Button) rootView.findViewById(R.id.btn_goFine);
        //미세먼지에 따라 미세먼지 아이콘 변경
        String finegrade = DataGetter.getFineGrade();
        if(finegrade=="좋음") btn_goFine.setBackgroundResource(R.drawable.good);
        else if(finegrade=="보통") btn_goFine.setBackgroundResource(R.drawable.so_so);
        else if(finegrade=="나쁨") btn_goFine.setBackgroundResource(R.drawable.bad);
        else if(finegrade=="매우나쁨") btn_goFine.setBackgroundResource(R.drawable.very_bad);
        else btn_goFine.setText("데이터 없음");

        btn_locationSelecet.setOnClickListener(this);
        btn_refresh.setOnClickListener(this);
        btn_goWeather.setOnClickListener(this);
        btn_goFine.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.btn_locationSelect:
                Intent intent = new Intent(getActivity().getApplication(), LocationSelectActivity.class);
                startActivity(intent);
                break;

            case R.id.btn_refresh:
                getFragmentManager().beginTransaction().detach(this).attach(this).commit();
                Log.d(TAG, "onClick: 새로고침");
                break;

            case R.id.btn_goWeather:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frag_container, new WeatherFragment()).commit();
                break;

            case R.id.btn_goFine:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frag_container, new FineDustFragment()).commit();
                break;

            default:
                break;
        }
    }
}
