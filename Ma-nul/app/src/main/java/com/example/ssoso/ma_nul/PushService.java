package com.example.ssoso.ma_nul;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.ssoso.ma_nul.Datas.AirPollution;
import com.example.ssoso.ma_nul.Datas.DataArrays;
import com.example.ssoso.ma_nul.Datas.Weather;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by ssoso on 2017-12-13.
 */

public class PushService extends Service {
    private ArrayList<AirPollution> airPollutions = new ArrayList<AirPollution>(); //Array list 하나 생성
    private ArrayList<Weather> weathers = new ArrayList<Weather>();

    @Override
    public IBinder onBind(Intent intent) {
        // Service 객체와 (화면단 Activity 사이에서)
        // 통신(데이터를 주고받을) 때 사용하는 메서드
        // 데이터를 전달할 필요가 없으면 return null;
        return null;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        // 서비스에서 가장 먼저 호출됨(최초에 한번만)
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // 서비스가 호출될 때마다 실행

        getAPIData();
        // push
         return super.onStartCommand(intent, flags, startId);
    }

    // API 데이터 받아오기
    private void getAPIData()
    {
        String[] curDate = getCurrent();
        String[] coord = getCoord();

        Log.d(getClass().toString(), "onResume: 좌표 : " + coord[0] + "," + coord[1]);

        new PushService.AirPollutionTask().execute("http://openapi.airkorea.or.kr/openapi/services/rest/ArpltnInforInqireSvc/getCtprvnRltmMesureDnsty?serviceKey=2gFEfP0Bi6cCbyg%2FUZZknocghGlMXW6dIp4oDlEbc5NO2FZL%2BW%2BUw2OaRXsJACqz3%2FeO05y9Zw5s9H6TIrajmQ%3D%3D&numOfRows=10&pageSize=10&pageNo=1&startPage=1&sidoName=제주&ver=1.3");
        new PushService.WeatherTask().execute("http://newsky2.kma.go.kr/service/SecndSrtpdFrcstInfoService2/ForecastSpaceData?serviceKey=2gFEfP0Bi6cCbyg%2FUZZknocghGlMXW6dIp4oDlEbc5NO2FZL%2BW%2BUw2OaRXsJACqz3%2FeO05y9Zw5s9H6TIrajmQ%3D%3D&base_date="+curDate[0]+"&base_time="+"0500"+"&nx="+coord[0]+"&ny="+coord[1]+"&numOfRows=10&pageSize=10&pageNo=1&startPage=1&_type=xml");
    }

    // 현재 x, y좌표 받음
    private String[] getCoord()
    {
        String[] Coord = new String[2];
        String location = LocationInfo.getLocation();

        if (location.equals("동흥동")) { Coord[0] = "53"; Coord[1] = "33"; }
        else if (location.equals("성산읍")) { Coord[0] = "60"; Coord[1] = "37"; }
        else if (location.equals("고산리")) { Coord[0] = "49"; Coord[1] = "37"; }
        else if (location.equals("연동")) { Coord[0] = "52"; Coord[1] = "38"; }
        else if (location.equals("이도동")){ Coord[0] = "53"; Coord[1] = "38"; }
        else { Coord[0] = "0"; Coord[1] = "0"; }

        return Coord;
    }

    // 현재 날짜와 시간 받음
    private String[] getCurrent()
    {
        long now = System.currentTimeMillis();
        Date date = new Date(now);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HHmm", java.util.Locale.getDefault());
        String str_date = dateFormat.format(date);

        Log.d(getClass().toString(), "getCurTime: 날짜시간 : " + str_date);

        String[] str = new String[2];
        str[0] = str_date.substring(0, 8);
        str[1] = str_date.substring(9, 13);

        return str;
    }

    class AirPollutionTask extends AsyncTask<String, Void, Void> {
        @Override//메인스레드
        protected void onPreExecute() {
            super.onPreExecute();
            airPollutions.clear();
        }

        @Override// 작업스레드 작업처리
        protected Void doInBackground(String... uris) {
            try {
                //네트워크 연동
                URL url = new URL(uris[0]);
                InputStream is = url.openStream();
                //InputStream is=getAssets().open("rss.xml");

                //XML 분석
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                Document doc = builder.parse(is);
                Element root = doc.getDocumentElement();
                //data 태그들을 찾아온다.
                NodeList nl = root.getElementsByTagName("item");

                //화면에 보여줄 데이터 구함
                for (int i = 0; i < nl.getLength(); i++) {
                    //각 data 태그에서  hour,temp,wfKor의 내용을 꺼내기
                    Element e = (Element) nl.item(i);
                    String location = getText(e, "stationName");
                    String[] values = new String[6];
                    String[] grades = new String[6];

                    values[0] = getText(e, "pm10Value");
                    values[1] = getText(e, "pm25Value");
                    values[2] = getText(e, "so2Value");
                    values[3] = getText(e, "coValue");
                    values[4] = getText(e, "o3Value");
                    values[5] = getText(e, "no2Value");

                    grades[0] = getText(e, "so2Grade");
                    grades[1] = getText(e, "coGrade");
                    grades[2] = getText(e, "o3Grade");
                    grades[3] = getText(e, "no2Grade");
                    grades[4] = getText(e, "pm10Grade1h");
                    grades[5] = getText(e, "pm25Grade1h");

                    for (int j = 0; j < grades.length; j++)
                    {
                        switch (grades[j])
                        {
                            case "1" : grades[j]="좋음"; break;
                            case "2" : grades[j]="보통"; break;
                            case "3" : grades[j]="나쁨"; break;
                            case "4" : grades[j]="매우나쁨"; break;
                            default: break;
                        }
                    }

                    Log.d(getClass().toString(), "doInBackground: " + location + values[0] + ", " + grades[4]);

                    AirPollution ap = new AirPollution(location, values, grades);
                    airPollutions.add(ap);
                }

            } catch (ParserConfigurationException e) {
                Log.d(getClass().toString(), "예외 " + e.getMessage());
            } catch (IOException e) {
                Log.d(getClass().toString(), "예외 " + e.getMessage());
            } catch (SAXException e) {
                Log.d(getClass().toString(), "예외 " + e.getMessage());
            }

            DataArrays.setAirPollutions(airPollutions);
            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            //작업 종료시에 화면 다시 표시
        }

        private String getText(Element e, String str)
        {
            return e.getElementsByTagName(str).item(0).getTextContent();
        }
    }

    class WeatherTask extends AsyncTask<String, Void, Void> {
        @Override//메인스레드
        protected void onPreExecute() {
            super.onPreExecute();
            airPollutions.clear();
        }

        @Override// 작업스레드 작업처리
        protected Void doInBackground(String... uris) {
            try {
                //네트워크 연동
                URL url = new URL(uris[0]);
                InputStream is = url.openStream();
                //InputStream is=getAssets().open("rss.xml");

                //XML 분석
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                Document doc = builder.parse(is);
                Element root = doc.getDocumentElement();
                //data 태그들을 찾아온다.
                NodeList nl = root.getElementsByTagName("item");

                //화면에 보여줄 데이터 구함
                for (int i = 0; i < nl.getLength(); i++) {
                    //각 data 태그에서  hour,temp,wfKor의 내용을 꺼내기
                    Element e = (Element) nl.item(i);
                    String category = getText(e, "category");
                    String fcstDate = getText(e, "fcstDate");
                    String fcstTime = getText(e, "fcstTime");
                    String fcstValue = getText(e, "fcstValue");
                    String nx = getText(e, "nx");
                    String ny = getText(e, "ny");

                    Log.d(getClass().toString(), "doInBackground: " + category + fcstValue);

                    Weather wt = new Weather(category, fcstDate, fcstTime, fcstValue, nx, ny);
                    weathers.add(wt);
                }

            } catch (ParserConfigurationException e) {
                Log.d(getClass().toString(), "예외 " + e.getMessage());
            } catch (IOException e) {
                Log.d(getClass().toString(), "예외 " + e.getMessage());
            } catch (SAXException e) {
                Log.d(getClass().toString(), "예외 " + e.getMessage());
            }

            DataArrays.setWeathers(weathers);
            return null;
        }

        private String getText(Element e, String str)
        {
            return e.getElementsByTagName(str).item(0).getTextContent();
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            //작업 종료시에 화면 다시 표시
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // 서비스가 종료될 때 실행
    }

}
