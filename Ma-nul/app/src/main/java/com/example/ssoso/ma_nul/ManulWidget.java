package com.example.ssoso.ma_nul;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

public class ManulWidget extends AppWidgetProvider {
    @Override
    public void onReceive(Context context, Intent intent)
    {
        super.onReceive(context, intent);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds)
    {
        // 현재 클래스로 등록된 모든 위젯의 리스트를 가져옴
        appWidgetIds = appWidgetManager.getAppWidgetIds(new ComponentName(context, getClass()));
        super.onUpdate(context, appWidgetManager, appWidgetIds);
         final int N = appWidgetIds.length;
        for(int i = 0 ; i < N ; i++)
        {
            int appWidgetId = appWidgetIds[i];
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    public static void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId)
    {
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.manul_widget);

        //위젯에 있는 아이콘 클릭 시 앱으로 이동
        Intent intent=new Intent(context, MainActivity.class);
        PendingIntent pe= PendingIntent.getActivity(context, 0, intent, 0);
        views.setOnClickPendingIntent(R.id.wicon_weather, pe);
        views.setOnClickPendingIntent(R.id.wicon_dust, pe);

        views.setTextViewText(R.id.widgettext, "test중입니다");
        views.setImageViewResource(R.id.wicon_weather,R.drawable.rainy);
        views.setImageViewResource(R.id.wicon_dust,R.drawable.nondata);

//        //위젯 날씨 아이콘 변경
//        String skystate = DataGetter.getSkyState();
//        String rainstate = DataGetter.getRainState();
//
//        if(rainstate=="비") views.setImageViewResource(R.id.wicon_weather,R.drawable.rainy);
//        else if(rainstate=="비/눈") views.setImageViewResource(R.id.wicon_weather,R.drawable.rainy);
//        else if(rainstate=="눈") views.setImageViewResource(R.id.wicon_weather,R.drawable.snowy);
//        else if(skystate=="맑음") views.setImageViewResource(R.id.wicon_weather,R.drawable.sunny);
//        else if(skystate=="구름조금") views.setImageViewResource(R.id.wicon_weather,R.drawable.cloudy);
//        else if(skystate=="구름많음") views.setImageViewResource(R.id.wicon_weather,R.drawable.cloud);
//        else if(skystate=="흐림") views.setImageViewResource(R.id.wicon_weather,R.drawable.cloud);
//        else views.setImageViewResource(R.id.wicon_weather,R.drawable.nondata);
//
//        //위젯 미세먼지 아이콘 변경
//        String finegrade = DataGetter.getFineGrade();
//
//        if(finegrade=="좋음") views.setImageViewResource(R.id.wicon_dust,R.drawable.good);
//        else if(finegrade=="보통") views.setImageViewResource(R.id.wicon_dust,R.drawable.so_so);
//        else if(finegrade=="나쁨") views.setImageViewResource(R.id.wicon_dust,R.drawable.bad);
//        else if(finegrade=="매우나쁨") views.setImageViewResource(R.id.wicon_dust,R.drawable.very_bad);
//        else views.setImageViewResource(R.id.wicon_dust,R.drawable.nondata);

        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}

